package com.cloudmall.cloudmallcoupon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CloudmallCouponApplication {

    public static void main(String[] args) {
        SpringApplication.run(CloudmallCouponApplication.class, args);
    }

}
