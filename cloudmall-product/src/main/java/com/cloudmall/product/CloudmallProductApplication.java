package com.cloudmall.product;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CloudmallProductApplication {

    public static void main(String[] args) {
        SpringApplication.run(CloudmallProductApplication.class, args);
    }

}
