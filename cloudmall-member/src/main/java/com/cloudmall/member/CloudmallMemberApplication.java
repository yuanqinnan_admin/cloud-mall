package com.cloudmall.member;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CloudmallMemberApplication {

    public static void main(String[] args) {
        SpringApplication.run(CloudmallMemberApplication.class, args);
    }

}
