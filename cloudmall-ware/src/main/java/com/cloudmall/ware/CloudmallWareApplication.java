package com.cloudmall.ware;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CloudmallWareApplication {

    public static void main(String[] args) {
        SpringApplication.run(CloudmallWareApplication.class, args);
    }

}
